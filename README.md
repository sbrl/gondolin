# gondolin

> The code behind my LoRaWAN-powered sensor station situated in my greenhouse. (Will) support store-and-forward sensor reading reports.

Work-in-progress C/C++ firmware for my Arduino-based sensor station for my greenhouse. Currently it collects:

 - Temperature and air pressure inside the box
 - Temperature and humidity outside the box but inside the greenhouse
 - Temperature and humidity outside both the box and the greenhouse
 - Soil moisture
 - Battery and Solar Panel voltage

The plan is to transmit this information via LoRaWAN to my home server. Unfortunately, my home server gets turned off at night - so I'm planning to build a store-and-forward system with acknowledgements to ensure that I don't lose any data.

Currently, only the firmware has been written. Once I've gotten to the point that the firmware is transmitting data to my home server, I'll write the server program and store that in here too.

Most likely it'll be a combination of a receiving daemon listening for messages on the LoRaWAN radio, a (shell script?) logger that logs the data to monthly CSV files (which I can keep for an arbitrarily long time), and (another shell script?) collectd module that imports the data into collectd so that I can get pretty graphs out with my web-based [collectd graph panel](https://github.com/pommi/CGP) (which deletes old stuff).

Once it's all set up, I'd like to investigate utilising machine learning to predict when the soil is going to dry out - and send me an appropriate reminder. I'd also like to play around with predicting other outputs too - such as the battery level, the temperature, and the weather. For this I'm probably going to use [brain.js](https://www.npmjs.com/package/brain.js).

## Parts
 - 1 box with transparent lid
 - **Microcontroller:** Arduino Uno _(I have one lying around, and my Wemos D1R2 is incompatible with my DHT22 - it keeps resetting! O.o)_
 - **Real time clock:** Adafruit PCF8523
 - **In-box temperature/air pressure sensor:** BMP280 (A BME280 would also give humidity - allowing the dew point to be calculated more accurately)
 - **External EEPROM:** 2 x AT24C64 (might need to daisy-chain more of these in - not sure yet)
 - **Out-of-box temperature/humidity sensor:** 2 x DHT22 (1 inside the greenhouse, and one outside)
 - **Soil Sensor:** 1 x Capacitive Soil Sensor v1.2 _(Capacitive sensors last longer, apparently)_
 - **Battery:** 1 x 3.7V 3400mAh lithium-ion battery _(from [nerdonic](https://nerdonic.com/))_
 - 1 x lithium-ion charge controller _(also from [nerdonic](https://nerdonic.com/))_
 - **Power timer:** Adafruit TPL5110 Breakout
 - **Power regulator:** 1 x MT3608 _(from [icstation](http://www.icstation.com/icstation-mt3608-step-converter-module-boost-converter-power-supply-module-p-3448.html))_
 - **Solar Panel:** 1 x 12V 2W Solar Panel _(I might add another one later if need be)_
 - 1 x [Maximum Power Point Tracker](https://www.solar-electric.com/learning-center/batteries-and-charging/mppt-solar-charge-controllers.html)
 - **Potential Dividers:** (Format: Resistor 1 / Resistor 2)
	 - **Solar panel:** 1MΩ / 1KΩ
	 - **Battery:** 1MΩ / 1MΩ
 - 1 x Normal-sized Breadboard
 - 1 x Mini Breadboard (for LoRa logic)
 - **LoRaWAN Chip:** RFM95
 - [RFM95 breakout board](https://www.tindie.com/products/leonahi/rfm95-lora-breakout-board/) _(Warning: It doesn't come with header pins!)_
 - 1 x [8-channel 3.3V-5V logic level converter](https://smile.amazon.co.uk/gp/product/B01FRQUQCS/ref=ox_sc_act_title_1?smid=A30L2KD589ZCAC&psc=1)
 - More cables than I can count :P


## Libraries
 - [LMIC](https://github.com/mcci-catena/arduino-lmic#pdfword-documentation)

#include <Arduino.h>

// Potential divider that measures the solar panel voltage
const int pin_divider_solar_panel = A4;
const float solar_panel_r1 = 1000000; // int's max value is 32K
const float solar_panel_r2 = 100000;
// Potential divider that measures the battery voltage
const int pin_divider_battery = A5;
const float battery_r1 = 1000000;
const float battery_r2 = 1000000;
// Input reference voltage
const float reference_voltage = 5;

// Other misc analogue sensors
const int pin_soil = A0;

// Digital sensors
const int pin_dhtA = 6;
const int pin_dhtB = 7;


#define SERIAL_LOG_ENABLED 1

// Reserve the first 256 bits (32 bytes) of the external eeprom for state
// information. The end of the ringbuffer is calculated automagically to be the 
// end of the shared devices.
#define EXT_EEPROM_RINGBUFFER_START 32
// The number of daisy-chained eeprom devices we have
#define EXT_EEPROM_DEVICE_COUNT 2

//// Core ///
#include <Arduino.h>

#include "bootstrap.h"

// ----------------------------------------------------------------------------

// The Arduino IDE build system is horible and inflexible - .cpp files should 
// really be specified as an argument to g++, but we can't do that I don't 
// think :-/

/// Libraries ///
#include "lib/RTClib/RTClib.h"
#include "lib/RTClib/RTClib.cpp"

/// Other files ///
#include "utils.h"
#include "readers.h"

// ----------------------------------------------------------------------------

RTC_PCF8523 rtc;


void setup() {
	Serial.begin(9600);
	SERIAL_PRINTLN("[main] Initialised serial connection.");
	SERIAL_PRINTLN("[main] *** Welcome to Gondolin ***");
	
	SERIAL_PRINTLN("[main] Entering Stage 1: Initalisation");
	// Real-time clock
	if(!rtc.begin()) {
		SERIAL_PRINTLN("Error: Failed to find the real-time clock!");
		halt();
	}
	if(!rtc.initialized()) {
		SERIAL_PRINTLN("Warning: Real-time clock is not intialised. Initialising from compile time.");
		// TODO: Send a request for the time via LoRaWAN instead
		rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
	}
	
	// BME 280
	setup_bme();
	
	SERIAL_PRINTLN("[main] Entering Stage 2: Immediate data collection");
	DateTime now = rtc.now();
	
	SERIAL_PRINT("[rtc] Current timestamp: ");
	SERIAL_PRINTLN_RAW(now.unixtime());
	
	read_power();
	read_bme();
	read_soil();
	
	// ------------------------------------------------------------------------
	
	SERIAL_PRINTLN("[main] Beginning Stage 3: Delayed data collection");
	
	log_line_begin();
	SERIAL_PRINTLN("[dht] Waiting for DHT22 to warm up.");
	delay(2000 - millis() + 1); // We've probably already done a bunch of work by now
	
	read_dht22();
	
	log_line_begin();
	SERIAL_PRINT("[main] Completed in ");
	SERIAL_PRINT_RAW(millis()); SERIAL_PRINTLN_RAW("ms");
}

void loop() {
	SERIAL_PRINTLN("Error: Reached loop!");
	delay(50000);
}

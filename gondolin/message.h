/*
Message format:
+----------+----~~~~~~~~~~~~~~~~~~~~~~+
| XXXXYYYY | ZZZZZZZZZZZZZZZZZZZZ.... <
+----------+----~~~~~~~~~~~~~~~~~~~~~~+

X {4-bit unsigned int}			The version number of the packet.
Y {4-bit unsigned int}			The message type.
Z {variable-length byte array}	The content of the message.
 */

/**
 * The message header spec.
 */
struct MessageHeader {
	unsigned int version : 4;
	unsigned int type : 4;
};

// An initialisation message, containing important info such as the build time of the client, and the timestamp to use for the first data message.
#define MESSAGE_TYPE_INIT 0
/*
 * A regular message containing one (or more) sensor readings.
 * Note that the timestamp is *not* sent with a data message. Instead, dead reckoning is used by the server to figure this information out.
 * Furthermore, since the length of a data packet is fixed, multiple data packets MAY be present in a single data message. It's up to the server to figure all this out and decode them.
 */
#define MESSAGE_TYPE_DATA 1


// FUTURE: We could implement delta compression and combine it with MSP variable-width integer encoding to reduce transmission message size. Supporting storage in such a format would be rather complex though, as the data block size would be variable-width.

/**
 * The format of a data message.
 */
struct DataMessage {
	// No need to store time in here, since we take readings every 10 minutes
	// This way we only need to store the time of the oldest message in storage
	unsigned int voltage_solar_panel : 9;
	unsigned int voltage_battery : 9;
	unsigned int voltage_soil : 10;
	unsigned int dhtA_temp : 10;
	unsigned int dhtA_humidity : 8;
	unsigned int internal_pressure : 13;
	unsigned int internal_temp : 15;
};

// TODO: Define min, max, and step values for all of the above readings
int value_pack(float f, float min, float step, float max);
int value_pack(int i, int min, int step, int max);

int message_send(byte* buffer, int size);

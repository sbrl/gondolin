
#include "lib/SimpleDHT/SimpleDHT.h"
#include "lib/SimpleDHT/SimpleDHT.cpp"

#include "lib/BME280/src/BME280.h"
#include "lib/BME280/src/BME280.cpp"
#include "lib/BME280/src/BME280I2C.h"
#include "lib/BME280/src/BME280I2C.cpp"
#include "lib/BME280/src/EnvironmentCalculations.h"
#include "lib/BME280/src/EnvironmentCalculations.cpp"

#include "bootstrap.h"
#include "utils.h"


SimpleDHT22 dht_client;
BME280I2C bme;

/*
██████   ██████  ██     ██ ███████ ██████
██   ██ ██    ██ ██     ██ ██      ██   ██
██████  ██    ██ ██  █  ██ █████   ██████
██      ██    ██ ██ ███ ██ ██      ██   ██
██       ██████   ███ ███  ███████ ██   ██
*/

void read_power() {
	float solar_panel = analog_voltage_parse(analogRead(pin_divider_solar_panel));
	float battery = analog_voltage_parse(analogRead(pin_divider_battery));
	solar_panel = voltage_divider_convert(solar_panel_r1, solar_panel_r2, solar_panel);
	battery = voltage_divider_convert(battery_r1, battery_r2, battery);
	
	SERIAL_PRINT("[power] Solar panel: "); SERIAL_PRINT_RAW(solar_panel); SERIAL_PRINT_RAW("V, Battery: "); SERIAL_PRINT_RAW(battery); SERIAL_PRINTLN_RAW("V");
}



/*
██████  ██   ██ ████████ ██████  ██████
██   ██ ██   ██    ██         ██      ██
██   ██ ███████    ██     █████   █████
██   ██ ██   ██    ██    ██      ██
██████  ██   ██    ██    ███████ ███████
*/

void read_dht22() {
	float tempA, humidityA, tempB, humidityB;
	int error_code = dht_client.read2(pin_dhtA, &tempA, &humidityA, NULL);
	if(error_code != SimpleDHTErrSuccess) {
		SERIAL_PRINT("[dht] Warning: Failed reading from DHT22 A. Code: ");
		SERIAL_PRINTLN_RAW(error_code);
	}
	error_code = dht_client.read2(pin_dhtB, &tempB, &humidityB, NULL);
	if(error_code != SimpleDHTErrSuccess) {
		SERIAL_PRINT("[dht] Warning: Failed reading from the DHT22 B. Code: ");
		SERIAL_PRINTLN_RAW(error_code);
	}
	
	SERIAL_PRINT("[dht] A: "); SERIAL_PRINT_RAW(tempA); SERIAL_PRINT_RAW("°C, "); SERIAL_PRINT_RAW(humidityA); SERIAL_PRINTLN_RAW("% rel. humidity");
	SERIAL_PRINT("[dht] B: "); SERIAL_PRINT_RAW(tempB); SERIAL_PRINT_RAW("°C, "); SERIAL_PRINT_RAW(humidityB); SERIAL_PRINTLN_RAW("% rel. humidity");
}


/*
██████  ███    ███ ██████  ██████   █████   ██████
██   ██ ████  ████ ██   ██      ██ ██   ██ ██  ████
██████  ██ ████ ██ ██████   █████   █████  ██ ██ ██
██   ██ ██  ██  ██ ██      ██      ██   ██ ████  ██
██████  ██      ██ ██      ███████  █████   ██████
*/

void setup_bme() {
	if(!bme.begin()) {
		SERIAL_PRINTLN("[bme] Warning: Faied to setup BME280");
		return;
	}
	
	switch(bme.chipModel()) {
		case BME280::ChipModel_BME280:
			SERIAL_PRINTLN("[bme] Found BME280 sensor! Success.");
			break;
		case BME280::ChipModel_BMP280:
			SERIAL_PRINTLN("[bme] Found BMP280 sensor! No Humidity available.");
			break;
		default:
			SERIAL_PRINTLN("[bme] Found UNKNOWN sensor! Error!");
			break;
	}
}

void read_bme() {
	float temp, humidity, pressure;
	BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
	BME280::PresUnit presUnit(BME280::PresUnit_hPa);
	
	bme.read(pressure, temp, humidity, tempUnit, presUnit);
	
	SERIAL_PRINT("[bme] "); SERIAL_PRINT_RAW(temp); SERIAL_PRINT_RAW("°C, "); SERIAL_PRINT_RAW(humidity); SERIAL_PRINT_RAW("% rel. humidity, "); SERIAL_PRINT_RAW(pressure); SERIAL_PRINTLN_RAW("hPa");
}


/*
███████  ██████  ██ ██          ███████ ███████ ███    ██ ███████  ██████  ██████
██      ██    ██ ██ ██          ██      ██      ████   ██ ██      ██    ██ ██   ██
███████ ██    ██ ██ ██          ███████ █████   ██ ██  ██ ███████ ██    ██ ██████
     ██ ██    ██ ██ ██               ██ ██      ██  ██ ██      ██ ██    ██ ██   ██
███████  ██████  ██ ███████     ███████ ███████ ██   ████ ███████  ██████  ██   ██
*/

void read_soil() {
	float parsedValue = analog_voltage_parse(analogRead(pin_soil));
	
	SERIAL_PRINT("[soil] "); SERIAL_PRINT_RAW(parsedValue); SERIAL_PRINTLN_RAW("V");
}

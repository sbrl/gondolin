#pragma once

// Bring in the settings
#include "settings.h"

/*
 * ███    ███  █████   ██████ ██████   ██████  ███████
 * ████  ████ ██   ██ ██      ██   ██ ██    ██ ██
 * ██ ████ ██ ███████ ██      ██████  ██    ██ ███████
 * ██  ██  ██ ██   ██ ██      ██   ██ ██    ██      ██
 * ██      ██ ██   ██  ██████ ██   ██  ██████  ███████
 */

#if SERIAL_LOG_ENABLED == 1
#define SERIAL_PRINTLN_RAW(str)		Serial.println(str)
#define SERIAL_PRINT_RAW(str)		Serial.print(str)
#else
#define SERIAL_PRINTLN_RAW(str)		{}
#define SERIAL_PRINT_RAW(str)		{}
#endif

#define SERIAL_PRINTLN(str)			log_line_begin(); SERIAL_PRINTLN_RAW(str)
#define SERIAL_PRINT(str)			log_line_begin(); SERIAL_PRINT_RAW(str)

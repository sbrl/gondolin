
#include <Arduino.h>

#include "bootstrap.h"

void halt() {
	while(true) delay(1000);
}

void log_line_begin() {
#if SERIAL_LOG_ENABLED == 1
	Serial.print("["); Serial.print((float)millis() / 1000.0); Serial.print("] ");
#endif
}

float analog_voltage_parse(int raw) {
	// reference_voltage is from settings.h
	return ((float)raw / 1024) * reference_voltage;
}

float voltage_divider_convert(float r1, float r2, float input) {
	return input / (r2 / (r1 + r2));
}
